<?php

namespace Drupal\video_filter\Plugin\VideoFilter;

use Drupal\video_filter\VideoFilterBase;

/**
 * Provides Slideshare codec for Video Filter.
 *
 * @VideoFilter(
 *   id = "slideshare",
 *   name = @Translation("Slideshare"),
 *   example_url = "https://slideshare.net/1759622",
 *   regexp = {
 *     "/slideshare\.net\/\?id=([a-z0-9]+)/",
 *     "/slideshare\.net\/([a-z0-9]+)/",
 *   },
 *   ratio = "425/355",
 * )
 */
class Slideshare extends VideoFilterBase {

  /**
   * {@inheritdoc}
   */
  public function html($video) {
    // Get embed code via oEmbed.
    $endpoint = 'https://www.slideshare.net/api/oembed/2?url=' . $video['source'] . '&format=json';
    $request = \Drupal::httpClient()->get($endpoint, ['headers' => ['Accept' => 'application/json']]);
    if ($request->getStatusCode() == 200) {
      $response = json_decode($request->getBody());
    }
    $html = !empty($response->html) ? $response->html : '';
    return $html;
  }

}
