<?php

namespace Drupal\video_filter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines base methods for all video filter plugin instances.
 */
class VideoFilterBase extends PluginBase implements VideoFilterInterface {

  use StringTranslationTrait;

  /**
   * Get plugin name.
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * Get plugin example URL.
   */
  public function getExampleUrl() {
    return $this->pluginDefinition['example_url'];
  }

  /**
   * Get plugin regexp.
   */
  public function getRegexp() {
    return $this->pluginDefinition['regexp'];
  }

  /**
   * Get video player ratio.
   */
  public function getRatio() {
    $ratio = !empty($this->pluginDefinition['ratio']) ? $this->pluginDefinition['ratio'] : '';
    if (!empty($ratio) && preg_match('/(\d+)\/(\d+)/', $ratio, $tratio)) {
      return (int) $tratio[1] / (int) $tratio[2];
    }
    return 1;
  }

  /**
   * Get video player control bar height.
   */
  public function getControlBarHeight() {
    return intval($this->pluginDefinition['control_bar_height'] ?? 0);
  }

  /**
   * Get Video Filter coded usage instructions.
   */
  public function instructions() {
    // Return text of the instruction for the codec.
    return '';
  }

  /**
   * HTML5 video (iframe).
   */
  public function iframe($video) {
    // Return HTML5 URL to the video, which will be passed into an iframe.
    return '';
  }

  /**
   * Flash video (flv).
   *
   * @deprecated in video_filter:8.x-1.3 and is removed from video_filter:2.0.0. No replacement provided.
   *
   * @see https://www.drupal.org/project/video_filter/issues/3433260
   */
  public function flash($video) {
    // Usually video URL that will be played with the FLV player.
    return '';
  }

  /**
   * HTML code of the video player.
   */
  public function html($video) {
    // Usually video URL that will be played with the FLV player.
    return '';
  }

  /**
   * Embed options. (e.g. Autoplay, Width/Height).
   *
   * Uses Drupal's Form API.
   */
  public function options() {
    $form['width'] = [
      '#title' => $this->t('Width (optional)'),
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => '400',
      ],
    ];
    $form['height'] = [
      '#title' => $this->t('Height (optional)'),
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => '400',
      ],
    ];
    return $form;
  }

  /**
   * Get video preview image.
   *
   * This video will be used in CKEditor.
   */
  public function preview($video) {
    // Returns absolute URL to preview image.
    return \Drupal::service('extension.list.module')->getPath('video_filter') . '/assets/preview.png';
  }

}
