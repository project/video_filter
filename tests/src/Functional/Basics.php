<?php

namespace Drupal\Tests\video_filter\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality of Video Filter module.
 *
 * @group video_filter
 */
class Basics extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'node',
    'filter',

    // This module.
    'video_filter',

    // Helper module.
    'video_filter_tests_helper',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Login.
    $this->drupalLogin($this->rootUser);

    // Create a simple content type.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
  }

  /**
   * Verify the shortcode filter.
   */
  public function testShortcode() {
    $format_name = 'test_video_filter';
    $format_label = 'Test Video Filter';

    // Load the node page.
    $this->drupalGet('node/add/page');

    // Confirm the video filter help text is visible.
    $this->assertSession()->responseContains('You may insert videos with [video:URL]');

    // Save the node with an example video.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName();
    $edit['body[0][value]'] = '[video:https://www.youtube.com/watch?v=U14IBek-wNU]';
    // $edit['body[0][format]'] = $format_name;
    $this->submitForm($edit, 'Save');

    // Make sure the node saved.
    $this->assertSession()->statusMessageContains('Basic page ' . $edit['title[0][value]'] . ' has been created.', 'status');

    // Make sure the video is embedded as expected.
    $this->assertSession()->responseContains('<iframe src="//www.youtube.com/embed/U14IBek-wNU');
  }

}
